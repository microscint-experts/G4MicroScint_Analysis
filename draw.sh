#!/bin/bash
# $1 - pass to data root file
# $2 - pass to mc root file
# Example: 			$ . draw.sh   ~/Templates/G4MicroScint/Data/ct0_scint_notape.root ~/Templates/G4MicroScint/MC/MC_tuning_chennel_groundness/rouphness78pct.root
root -l -b -q StartDraw.C+\(\"$1\"\,\"$2\"\)


# For Data/MC in the note:
# . draw.sh   ~/Templates/G4MicroScint/Data/ct0_scint_notape.root ~/Templates/G4MicroScint/MC/MC_tuning_plate_surface/notape.root