#include <time.h>
#include <string>
#include <iostream>
#include <map>
#include <sstream>
#include <fstream>
#include <vector>
#include <stdlib.h>
#include <iomanip>

#include <TString.h>
#include <TSystem.h>
#include <TInterpreter.h>
#include <TXMLEngine.h>
#include <TCanvas.h>
#include <TFile.h>
#include <TH1.h>
#include <TH2.h>
#include <TH3.h>
#include <TStyle.h>
#include <TLatex.h>
#include <TROOT.h>
#include <THStack.h>
#include <TKey.h>
#include <TF1.h>
#include <TError.h>
#include <TLegend.h>
#include <TPaletteAxis.h>
#include <TSpectrum.h>
#include <TPolyMarker.h>


TString GetUnionPDFName()
{
  return TString("plots/AllPlots.pdf");
}

int GetNiceColor(const unsigned int nHist)
{
	std::vector<int> vColor = { kRed, kBlue+2, kOrange-3, kGreen+2, kCyan+2, kMagenta+2, 8, 28, 7, kYellow+2 }; 
	
	if(nHist>=vColor.size()) {
		Error("GetNiceColor", "You are asking color for nHist=%d, but %d is a limit. Will use additional ugly colors set.", nHist, (int)vColor.size());
		std::vector<int> vMoreColor = {1,2,3,4,5,6,7,8,9};
		unsigned int newNHist = nHist-vColor.size();
		if(newNHist>=vMoreColor.size())
			Fatal("GetNiceColor", "You are asking color for newNHist=%d, but %d is a limit.", newNHist, (int)vMoreColor.size());

		return vMoreColor.at(newNHist);
	}

	return vColor.at(nHist);
}

bool IsPassPattern(TString histName, std::vector<TString> vSkipPattern)
{
	for(auto pattern : vSkipPattern){
		if(histName.Contains(pattern)) return false;
	}
	return true;
}

std::pair<double, double> GetPhotonsLimit()
{
	return std::make_pair(1, 40);
}

TH1* GetHistFromRootFile(TString dataFileName = "passToDataRootFile", TString histName = "unicHistName") 
{
	TFile* file = 0x0;
	if(!(file = TFile::Open(dataFileName))) {
		Error("GetDataHist()", "File %s pointer in 0x0.", dataFileName.Data());
		return 0x0;
	}
	if(!file->IsOpen()) {
		Error("GetDataHist()", "For some reason file %s is closed.", dataFileName.Data());
		return 0x0;
	}

	return (TH1*)file->Get(histName);
}

std::vector<Double_t> SearchHR1(TH1* h) 
{
	std::vector<Double_t> vOut;

	Double_t fPositionX[100];
	Double_t fPositionY[100];
	Int_t fNPeaks = 0;
	Int_t i,nfound,bin;
	Double_t a = 0;
	Double_t nbins = h->GetNbinsX();
	Double_t xmin  = 0;
	Double_t xmax  = nbins;
	Double_t * source = new Double_t[(int)nbins];
	Double_t * dest = new Double_t[(int)nbins];
	TH1F *d = new TH1F("d","",nbins,xmin,xmax);
	for (i = 0; i < nbins; i++) source[i]=h->GetBinContent(i + 1);
	TString canvasName = "SearchHR1";
		TCanvas* Search = new TCanvas("Search","Search",10,10,1000,700);
	h->SetMaximum(4000);
	h->Draw("L");
	TSpectrum *s = new TSpectrum();
	nfound = s->SearchHighRes(source, dest, nbins, 3, 2, kTRUE, 10, kTRUE, 3);
	Double_t *xpeaks = s->GetPositionX();
	for (i = 0; i < nfound; i++) {
	  a=xpeaks[i];
	  bin = 1 + Int_t(a + 0.5);
	  fPositionX[i] = h->GetBinCenter(bin);
	  fPositionY[i] = h->GetBinContent(bin);
	}
	TPolyMarker * pm = (TPolyMarker*)h->GetListOfFunctions()->FindObject("TPolyMarker");
	if (pm) {
	  h->GetListOfFunctions()->Remove(pm);
	  delete pm;
	}
	pm = new TPolyMarker(nfound, fPositionX, fPositionY);
	h->GetListOfFunctions()->Add(pm);
	pm->SetMarkerStyle(23);
	pm->SetMarkerColor(kRed);
	pm->SetMarkerSize(1.3);
	for (i = 0; i < nbins; i++) d->SetBinContent(i + 1,dest[i]);
	d->SetLineColor(kRed);
	// d->Draw("SAME");
	Search->SaveAs(Form("plots/png/%s.png",canvasName.Data()));
	Search->SaveAs(Form("plots/png/%s.eps",canvasName.Data()));
	// Search->Print(GetUnionPDFName());
	printf("Found %d candidate peaks\n",nfound);
	for(i=0;i<nfound;i++) {
	  // printf("posx= %f, posy= %f\n",fPositionX[i], fPositionY[i]);
	  vOut.push_back(fPositionX[i]);
	}
	std::sort (vOut.begin(), vOut.end());
	return vOut;
}

TH1* ConvertEnergyToNPhotons(TH1* histData) 
{
	Double_t nbins = GetPhotonsLimit().second;
	Double_t xmin  = 0;
	Double_t xmax  = nbins;
	TH1F* resultHist = new TH1F("resultHist","resultHist",nbins,xmin,xmax);

	std::vector<Double_t> peakPositions  = SearchHR1(histData);
	// for(auto position : peakPositions)
	// 	printf("Peak: %f\n", position);

	double meanWidth = 0;
	double nPeaks = 0;
	double lastPeak = peakPositions.at(0);
	for(auto position : peakPositions) {
		nPeaks++;
		meanWidth += (position - lastPeak);
		lastPeak = position;
	}
	meanWidth= meanWidth/nPeaks;
	printf("meanWidth: %f\n", meanWidth);

	// Now lets cut original hist and integrate

	Double_t position = peakPositions.at(0);
	nPeaks = 1;
	while(true) {
		Double_t minLim = position-meanWidth/2.;
		Double_t maxLim = position+meanWidth/2.;

		if(minLim<0) minLim=0;

		Int_t minBin = histData->FindBin(minLim);
		Int_t maxBin = histData->FindBin(maxLim);

		Double_t integral = histData->Integral(minBin,maxBin);

		printf("minLim: %f, maxLim: %f, integral: %f\n", minLim, maxLim, integral);

		resultHist->SetBinContent(nPeaks, integral);

		position = position + meanWidth;
		nPeaks++;
		if(nPeaks>=nbins || nPeaks>=40) break;
	}

	return resultHist;
}

void DrawRatioSubPlot(TH1* histData, TH1* histMC, float integralMC = 0., float setRangeA_Y = 0., float setRangeB_Y = 0., float setRangeA_X = 0., float setRangeB_X = 0.)
{
	if(!histData){
		cout<<"DrawRatioSubPlot:: Fatal error. Pointer histData = 0x0"<<endl;
		exit(1);
	}
	if(!histMC){
		cout<<"DrawRatioSubPlot:: Fatal error. Pointer histMC = 0x0"<<endl;
		exit(1);
	}
	if(!integralMC) {
		cout<<"DrawRatioSubPlot:: Fatal error. integralMC = 0"<<endl;
		exit(1);
	}

  	TH1* h_ratio = (TH1*)histData->Clone("dataRatioClone");
    // h_ratio->Divide(histMC);
    // histData->Divide(histMC);
    // TH1F* h_ratio = histData;
    float rel_uncert_from_data_histo, rel_uncert_from_MC_histo, rel_uncert_overall;
    for (int i=1;i<=(h_ratio->GetNbinsX());i++)
    {
    	h_ratio->SetBinContent(i, histData->GetBinContent(i)/histMC->GetBinContent(i));

		// printf("A: %f, B: %f, A/B: %f\n", histData->GetBinContent(i), histMC->GetBinContent(i), h_ratio->GetBinContent(i));
		rel_uncert_from_data_histo = (histData->GetBinContent(i) == 0)?0.:histData->GetBinError(i)/histData->GetBinContent(i);
		rel_uncert_from_MC_histo   = (histMC->GetBinContent(i) == 0)?0.:histMC->GetBinError(i)/(integralMC)/histMC->GetBinContent(i);
		rel_uncert_overall = sqrt(pow(rel_uncert_from_data_histo,2)+pow(rel_uncert_from_MC_histo,2));
		h_ratio->SetBinError(i,rel_uncert_overall*h_ratio->GetBinContent(i));
    }
    h_ratio->GetXaxis()->SetTitle(histData->GetXaxis()->GetTitle());
    h_ratio->GetYaxis()->SetTitle("Data/MC");
    h_ratio->GetYaxis()->SetTitleSize(0.15);
    h_ratio->GetYaxis()->SetTitleOffset(0.4);
    h_ratio->GetXaxis()->SetTickLength(0.06);
    h_ratio->GetXaxis()->SetTitleSize(0.12);
    h_ratio->GetXaxis()->SetTitleOffset(0.5);
    h_ratio->GetYaxis()->SetTitleSize(0.15);
    h_ratio->GetYaxis()->SetTitleOffset(0.4);
    h_ratio->GetXaxis()->SetTickLength(0.06);
	h_ratio->GetXaxis()->SetTitleSize(0.12);
	h_ratio->GetXaxis()->SetTitleOffset(0.5);
    h_ratio->GetXaxis()->SetLabelSize(0.1); 
    h_ratio->GetYaxis()->SetLabelSize(0.1); 

    if(setRangeA_Y!=setRangeB_Y) { // set specific range for Y axis
    	h_ratio->GetYaxis()->SetRangeUser(setRangeA_Y,setRangeB_Y);
    }
    else { // set maximum as maximum Y point +10% of full width,
           // set minimum as minimum Y point -10% of full width
		float minValue = h_ratio->GetBinContent(h_ratio->GetMinimumBin());
		float maxValue = h_ratio->GetBinContent(h_ratio->GetMaximumBin());

		float width = fabs(maxValue-minValue);

		float c = 0.05; // 10%

		h_ratio->GetYaxis()->SetRangeUser(minValue-c*width,maxValue+c*width);
    }

    h_ratio->Draw("PE");

    // TF1* fit = new TF1("fitOfRatio","[0]",1,39);
    // fit->SetParameter(0,2);
    // fit->SetLineColor(kRed);
    // h_ratio->Fit(fit,"R");

    // Draw line
    if(setRangeA_X==setRangeB_X) {
		float minX = h_ratio->GetXaxis()->GetXmin();
		float maxX = h_ratio->GetXaxis()->GetXmax();
		TLine *l=new TLine(minX, 1., maxX, 1.);
		l->SetLineColor(kBlack);
		l->SetLineStyle(2);
		l->Draw();
    }
    else {
		TLine *l=new TLine(setRangeA_X, 1., setRangeB_X, 1.);
		l->SetLineColor(kBlack);
		l->SetLineStyle(2);
		l->Draw();
    }
}

int GetNumberOfSiPMs() {
	// TODO: Make script to scan current root file to find correct number out of box.
	// Now, simple solution:
	return 4; // [0, 3]
}