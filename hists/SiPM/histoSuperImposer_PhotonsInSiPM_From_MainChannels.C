TLegend* BuildLegend_PhotonsInSiPM_From_MainChannels (std::vector<std::pair<double,TH1*>> vStack);

//=============================

void histoSuperImposer_PhotonsInSiPM_From_MainChannels(int iSiPM = 0, TString dataFileName = "passToDataRootFile")
{
	TString histoname = Form("hSiPM_%d_PhotFrom_", iSiPM);
	int rebinX_coeff = 1;

	std::vector<TString> vSkipPattern = {"_SCh_", "_World", "_SiPM_", "_Collimator"};

	TCanvas* canvas = new TCanvas(histoname,"c1",0.,0.,800,600);
	canvas->SetLogy();
	canvas->SetGrid();
	canvas->RedrawAxis();
	gStyle->SetOptStat(0);

	THStack *hStack = new THStack("hs",";N_{#gamma};1/N dN/dN_{#gamma}");
	std::vector<std::pair<double,TH1*>> vStack; // integral and hist

	TFile* file = 0x0;
	if(!(file = TFile::Open(dataFileName)))
		return;
	
	file->cd();
	TIter next(file->GetListOfKeys()); 
	TKey *key; 
	int nhist = 0; 
	while ((key = (TKey*)next())) {  
		TString histName = key->GetName();
		if(!histName.Contains(histoname)) continue;
		if(!IsPassPattern(histName, vSkipPattern)) continue; 

		TH1* tmpHist = (TH1*)file->Get(histName);

		tmpHist->Rebin(rebinX_coeff);
		tmpHist->SetLineColor(GetNiceColor(nhist));
		tmpHist->SetLineWidth(2);

		// Divide to integral
		float integral = tmpHist->Integral(GetPhotonsLimit().first,GetPhotonsLimit().second);
		// tmpHist->Scale(1./integral);
		
		std::pair<double,TH1*> pair = std::make_pair(integral,tmpHist);
		vStack.push_back(pair);

		hStack->Add(tmpHist);
		nhist++;
	} 

	// sort by intergal
	sort(vStack.begin(), vStack.end()); 

	float maxY = 10.;
	float minY = 0.1;
	bool firstPlot_Range = true;
	for(auto hPair: vStack){
		TH1* h = hPair.second;
		if(firstPlot_Range){
			maxY = h->GetMaximum();
			firstPlot_Range = false;
		}
		else {
			if(maxY < h->GetMaximum()) 
				maxY = h->GetMaximum();
		}
	}

	bool firstPlot = true;
	for(auto hPair: vStack) {
		TH1* h = hPair.second;
		if(firstPlot){
			h->GetXaxis()->SetRangeUser(GetPhotonsLimit().first,GetPhotonsLimit().second);
			// h->GetXaxis()->SetRangeUser(0,GetPhotonsLimit().second);
			h->GetYaxis()->SetRangeUser(minY,maxY);
			TH1* hC = (TH1*)h->Clone();
			hC->SetTitle(";N_{#gamma};1/N dN/dN_{#gamma}");
			hC->SetFillColor(h->GetLineColor()-1);
			hC->Draw("HIST");
			firstPlot = false;
		}
		else
			h->Draw("sameHIST");
	}


	BuildLegend_PhotonsInSiPM_From_MainChannels(vStack);



	// hStack->Draw("nostackHIST");
	// hStack->GetXaxis()->SetRangeUser(GetPhotonsLimit().first,GetPhotonsLimit().second);
	// hStack->Draw("nostackHIST");

	// canvas->BuildLegend();

	canvas->RedrawAxis();
	
	canvas->SaveAs(Form("plots/png/%s.png",histoname.Data()));
	canvas->Print(GetUnionPDFName());
}

TLegend* BuildLegend_PhotonsInSiPM_From_MainChannels (std::vector<std::pair<double,TH1*>> vStack) 
{

	for(auto hPair : vStack){
		TH1* h = hPair.second;
		TString title = TString(h->GetTitle());
		std::cout<<title<<std::endl;

		if(title.Contains("MCh_")){
			title = TString("#gamma born in MCh_")+ TString(title[20]);
		} else
		if(title.Contains("Plate")){
			title = TString("#gamma born in Plate");
		}

		std::cout<<title<<std::endl;
		h->SetTitle(title);

	}

	// actual legend
	TLegend* leg = new TLegend(0.65,0.7,0.93,0.93);
	for(auto hPair : vStack){
		TH1* h = hPair.second;
		leg->AddEntry(h,h->GetTitle(),"pl");
	}
	leg->SetFillColor(0);
	leg->SetBorderSize(1);
	leg->SetTextFont(42);
	leg->SetTextSize(0.04);
	leg->Draw();

	return leg;
}