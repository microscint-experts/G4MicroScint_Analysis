#!/bin/bash

find . -type f -name '*.pdf' -delete
find . -type f -name '*.png' -delete
find . -type f -name '*.root' -delete | grep Calibrations/files/

echo "Folders are clean!"